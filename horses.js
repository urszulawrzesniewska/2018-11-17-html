var horses = [
  {
        "name": "Pinkie Pie",
        "type": "pony",
        "colour": "pink",
        "cover": "img/pinkiepie.jpg",
        "age": "1",
        "place": "Equestria",
        "birthday": "2018",
        "ponyshows": [
            2018, 2019
        ],
    },
{
        "name": "Rainbow Dash",
        "type": "pony",
        "colour": "blue",
        "cover": "img/rainbowdash.jpg",
        "age": "2",
        "place": "Equestria",
        "birthday": "2017",
        "ponyshows": [
         2017, 2018, 2019
        ],
    },
    {
        "name": "Fluttershy",
        "type": "pony",
        "colour": "yellow",
        "cover": "img/fluttershy.jpg",
         "age": "3",
        "place": "Equestria",
        "birthday": "2016",
        "ponyshows": [
         2016, 2017, 2018, 2019
            ],
        },
        {
        "name": "Princess Celestia",
        "type": "pony",
        "colour": "white",
        "cover": "img/celestia.jpeg",
        "age": "4",
        "place": "Equestria",
        "birthday": "2015",
        "ponyshows": [
         2015, 2016, 2017, 2018, 2019
                ],
            },
];

  function populateHorse(container) {
   var container = document.getElementById(container);
   for (var index in horses) {
       horse = horses[index];
       var a = document.createElement("a");
       var img = document.createElement("img");
       console.log(horse);

       img.alt = horse.name;

       img.src = horse.cover;

       img.classList.add('cover');
       a.href = 'list.html?horses=' + index;
       a.appendChild(img);
       container.appendChild(a);
   }
}

    function populateHorses(container) {
   var url_string = window.location.href;
   var url = new URL(url_string);
   var indexhorse = url.searchParams.get("horses");
   var container = document.getElementById(container);
   horse = horses[indexhorse];
   article = document.createElement("article");
   container.appendChild(article);


 /*  name = createHeader('h2', horse.name);
   article.appendChild(name);*/


   coverAside = document.createElement("aside");
   article.appendChild(coverAside);
   coverHeader = createHeader('h4', 'Cover');
   coverAside.appendChild(coverHeader);
   coverImg = createImg(horse.name, horse.cover, 'cover');
   coverAside.appendChild(coverImg);
   aboutSection = createSection("Pony details");
   article.appendChild(aboutSection);
   var horsesTable = createHorsesTable(horse);
   article.appendChild(horsesTable);
   footer = document.createElement("footer");
   article.appendChild(footer);
}
function createImg(alt, src, clazz) {
   coverImg = document.createElement("img");
   coverImg.alt = alt;
   coverImg.src = src;
   coverImg.classList.add(clazz);
   return coverImg;
}
function createHorsesTable(horse) {
   var table = document.createElement("table");

   table.classList.add("horsesTable");

   table.appendChild(createSimpleRow("name", horse.name));
   table.appendChild(createSimpleRow("type", horse.type));
   table.appendChild(createSimpleRow("colour", horse.colour));
   table.appendChild(createSimpleRow("age", horse.age));
   table.appendChild(createSimpleRow("place", horse.place));
   table.appendChild(createSimpleRow("birthday", horse.birthday));
   ponyshows = createSpanRows("pony shows", horse.ponyshows);
   for (ponyshow in ponyshows) {
table.appendChild(ponyshows[ponyshow]);
   }    return table;
}

function createSimpleRow(header, value) {
   var tr = document.createElement("tr");
   var th = document.createElement("th");
   th.innerHTML = header;
   tr.appendChild(th);
   var td = document.createElement("td");
   td.innerHTML = value;
   tr.appendChild(td);
   return tr;
}
function createSpanRows(header, value) {
   var trs = [];
   var first = true;
   for (var v in value) {
       var tr = document.createElement("tr");
       if (first) {
           var th = document.createElement("th");
           th.innerHTML = header;
           th.rowSpan = value.length;
           tr.appendChild(th);
           first = false;
       }
       var td = document.createElement("td");
       td.innerHTML = value[v];
       tr.appendChild(td);
       trs.push(tr);
   }
   return trs;
}
function createSection(name) {
    var section = document.createElement("section");
    section.appendChild(createHeader("h4", name));
    return section;
}

function createHeader(h, text) {
    header = document.createElement("header");
    h = document.createElement(h);
    h.innerHTML = text;
    header.appendChild(h);
    return header;
}
